//
//  main.c
//  C Programming
//
//  Created by Tevon Wallace on 4/1/19.
//  Copyright © 2019 Tevon Wallace. All rights reserved.
//

// Include statements are used to import the libraries that you wish to use in the project
#include <stdio.h>

/*
    There are two formats to create functions in C:
    
    function prototypes and function definition
    
    If you create a function and it located below the main() method/function, you have to declare the function above the main method so it's visible
    to other functions
    
    You can also create a function without function prototype above the main method and it will execute just fine.
    
    FUNCTION PROTOTYPE
    returntype functionName(function parameters)
    
    
    Eg: int someFunctionName(int, int);
    
    FUNCTION DEFINITION - This is the function prototype for the definition above
    int someFunctionName(int variable1, int variable2) {
        
    }
*/

void testFunction() {
    printf("Hello, World from testFunction()\n");
}

void whatsMyName(char *name) {
    printf("\nYour name is: %s\n", name);
}

int main(int argc, const char * argv[]) {
    char *name = "Tevon Wallace";
    
    printf("First C Project. Thank you!\n"); // This code will be displayed to the user
    // printf() is used to display messages to the user
    
    // Theses are the basics that I will demonstrate in this C Tutorial
    // The items below will be demonstrated in the order displayed
    
    /*
     1 -> Functions
     1.5 -> Datatypes
     2 -> Structures
     3 -> Enumerations
     4 -> Input and Outputs (display and accept input from the user)
     4.5 -> Conditional Statements (if, else and switch)
     5 -> Unions
     6 -> Pointers
     7 -> Files
     */
    
    testFunction();
    
    whatsMyName(name);
    
    return 0; // Since this main function has a return type of 'int'. The program will execute just fine without the return statement
}
